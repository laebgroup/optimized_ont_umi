import pandas as pd
import os
import argparse


def parse_clusters(min_reads: int, max_reads: int, smolecule_out: str, stats_out: str, vsearch_clusters: str):
	"""
	parse the output from vsearch to identify clusters that fulfill our criteria
	:param min_reads: the minimum number of reads for a cluster to be carried further
	:param max_reads: the maximum number of reads that will be written. Should ideally be divisible by 2
	:param smolecule_out: output from function that will be used as input for medaka smolecule
	:param stats_out: some stats about the clusters
	:param vsearch_clusters: the input file from vsearch (generacted with --uc)
	:return:
	"""
	# read in the vsearch "--uc" file
	# index is set to column 1 which is the cluster id
	vsearch = pd.read_csv(vsearch_clusters, sep="\t", header=None)
	vsearch = vsearch[vsearch[0] != 'C']  # take out the cluster info lines. They contain no important info and are not real reads.
	vsearch[3] = vsearch[3].replace('*', 100).astype(float)  # convert the similarity percentage to a float (its string because C and S records have a * in this column)
	vsearch['read'] = vsearch[8].str.split(';seq=', expand=True)[1]  # why is strand (reported by extract_umis.py) different from vsearch output column 4? We stick to original script here
	vsearch['strand'] = vsearch[8].str.contains('strand=+', regex=False)

	# if the output file already exists, then delete it
	if os.path.exists(smolecule_out):
		os.remove(smolecule_out)

	cluster_stats = []  # for creating a file with cluster summary statistics. 1 row per cluster
	# {
	# 'cluster_id': unique id per cluster,
	# 'n': number of cluster members,
	# 'cluster_acc': median hit alignment accuracy
	# 'n_fwd': number of forward reads
	# 'n_rev': number of reverse reads
	# 'cluster_written': 1 if cluster is processed further, otherwise 0
	# 'written_fwd: number of forward reads written
	# 'written_rev: number of reverse reads written
	# 'written': total number of reads written
	# }

	with open(smolecule_out, 'w') as smol:
		for cid, cluster in vsearch.groupby(1):
			n_fwd = (cluster['strand'] == True).sum()
			n_rev = (cluster['strand'] == False).sum()
			write_it = len(cluster) >= min_reads
			written_fwd = 0
			written_rev = 0
			# ideally we write an equal amount of fwd and rev reads, but if that's not possible then write as many as possible up to max_reads
			if write_it:
				if n_fwd >= max_reads / 2 and n_rev >= max_reads / 2:  # there are plenty of reads so just pick max_reads/2 random fwd and rev reads
					written_fwd = int(max_reads / 2)
					written_rev = max_reads - written_fwd  # this is done like this in case max_reads is not divisible by 2
				elif n_fwd >= max_reads / 2:  # there not enough rev reads to max it out, then take as many rev reads as possible and fill up with fwd reads
					written_rev = n_rev
					written_fwd = min(max_reads - n_rev, n_fwd)  # try to fill out with fwd reads if there are enough
				elif n_rev >= max_reads / 2:  # same as above, but switch fwd and rev
					written_fwd = n_fwd
					written_rev = min(max_reads - n_fwd, n_rev)
				else:  # neither fwd nor rev can fill half the quota, so just take everything
					written_fwd = n_fwd
					written_rev = n_rev

				# actually write
				fwd_cluster = cluster.loc[cluster['strand'] == True, 'read'].sample(written_fwd)
				rev_cluster = cluster.loc[cluster['strand'] == False, 'read'].sample(written_rev)
				cluster_reads = pd.concat([fwd_cluster, rev_cluster], axis=0).reset_index(drop=True)

				for i, item in enumerate(cluster_reads):
					smol.write(f'>{cid}_{i}\n{item}\n')

			cluster_summary = {
				'cluster_id': cid,
				'n': len(cluster),
				'cluster_acc': cluster[3].median(),
				'n_fwd': n_fwd,
				'n_rev': n_rev,
				'cluster_written': write_it,
				'written_fwd': written_fwd,
				'written_rev': written_rev,
				'written': written_fwd + written_rev
			}
			cluster_stats.append(cluster_summary)

	cluster_stats = pd.DataFrame(cluster_stats)
	cluster_stats = cluster_stats.sort_values('n', ascending=False)
	cluster_stats.to_csv(stats_out, sep='\t', index=False)


if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('--vsearch_clusters', type=argparse.FileType('r'), help="uc file outputtet by vsearch")
	parser.add_argument('--max_reads', type=int, default=60, help="Downsize to N reads per cluster")
	parser.add_argument('--min_reads', type=int, default=20, help="minimum size of clusters. Clusters with less reads will be discarded")
	parser.add_argument('--stats_out', default="/dev/null", help="Output location for stats file. File contain information about each clusters (id, size, accuracy, etc.)")
	parser.add_argument('--smolecule_out', default="/dev/null", help="input file for medaka smolecule")
	args = parser.parse_args()

	parse_clusters(args.min_reads, args.max_reads, args.smolecule_out, args.stats_out, args.vsearch_clusters)
