# What?

This repo contains a set of files that slightly alter the running of ONTs umi amplicon pipeline (https://github.com/nanoporetech/pipeline-umi-amplicon/)

You should install ONTs pipeline and then replace the Snakefile and config.yml with the ones in this repo.  
The new snakefile will also call parse_clusters_new.py to either move it to your Snakefile directory or alter Snakefile with an absolute path for it

# Tricks!
ONTs conda environment is using some outdated packages. If you prefer to run it with more updates ones here are some tricks:
1. You must use python 3.10 because medaka currently depends on tensorflow 2.8 which doesnt work on python >3.10
2. Medaka requires pyspoa which on my computer wont install directly from pip, but I can [build from source](https://github.com/nanoporetech/pyspoa). If you don't want to build it yourself, maybe you can get away with installing directly via pip, but otherwise you can also use `sudo apt install python3-pyspoa`. This will then require you to use global packages in python
3. We just need their scripts, not their snakefile or environment because we provide optimized snakefile here. Just clone their repo, goto the lib folder and `pip install .`
4. You will need the following tools on path
   - minimap2 (tested with 2.26)
   - samtools (tested with 1.17)
   - bedtools (tested with 2.31.0)
   - vsearch (tested with 2.22.1)
   - varscan (2.4.3, not actually used in our pipeline, but if you want to use pipeline-umi-amplicon to call variants you need it)
   - seqkit (tested on 2.4.0)
   - tabix (tested with 1.17)
   - In Ubuntu (and probably other linux distributions) they can be installed like this:
     - `sudo apt install minimap2 samtools bedtools vsearch varscan seqkit tabix`
     - Notice, seqkit (and possible some of the others) is only available from Ubuntu 22.04 onwards, so you may also consider just installing the lastest versions of the tools manually.
5. In addition to the python packages in the ONT repo you can install the latest of the following:
   - medaka
   - snakemake
   - numpy
   - scikit-learn
   - biopython
   - edlib